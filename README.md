# SHM_2024_Cu3PdN

## PDF processing code used for manuscript "Cation-site Disordered Cu3PdN Nanoparticles for Hydrogen Evolution Electrocatalysis"
The code his written by Sani Y. Harouna-Mayer. In case of any questions please contact: <sani.harouna-mayer@uni-hamburg.de>.

## Authors
Sani Y. Harouna-Mayer°, Jagadesh Kopula Kesavan°', Francesco Caddeo°, Lian Belgardt, Chia- Shuo Hsu, Lars Klemeyer, Lizzi Kipping, Melike Gumus Akcaalan, Tjark R.L. Groene, Andrea Koeppen, Olivier Mathon, Ann-Christin Dippel, Dorota Koziej'

°S.Y. Harouna-Mayer, J. Kopula Kesavan and F. Caddeo contributed equally.

## Description
This repository contains code for the processing of the total x-ray scattering (TXS) data for pair distribution function (PDF) analysis including azimuthal integration of detector images, background subtraction approaches, averaging, Fourier transforming and PDF refinement procedures. The processing is explained in detail in the supporting information of the manuscript.

### Files
- `process_raw_iq.py` background subtraction and processing of 1D scatterign data (iq) to PDFs
- `azimuthal_integration_2d_detector_images.py` azimuthal integration of 2d scattering images to 1s scatterin patterns using pyFAI
- `pdf_refinement_phasetransition_by_N_occupancy.py` in situ pdf refinement of the phase transition of Cu3PdN to Cu3Pd using diffpy-CMI